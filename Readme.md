1. Installation

npm install @marcin.rulkowski/syslog-udp-client

2. Overview

[ ![Codeship Status for mrulkowski/node-syslog-udp-client](https://codeship.com/projects/81283280-00bb-0133-14c7-46d3771abf46/status?branch=master)](https://codeship.com/projects/88435)

Module provide udp write stream and message formatter to send logs to syslog server from bunyan logger.
 
Example usage:

```javascript
      logger = bunyan.createLogger({
        name: 'logger-name',
        streams: [{
          type: 'raw',                          
          level: 'info',
          stream: bunyanStream.createStream({
            host: HOST,
            port: PORT,
            appName: 'application-name',
            levelMapping: function () {
              return syslogLevel.DEBUG;
            }
          })
        }]
      });
```

It's mandatory that type of stream is marked fo bunyan as a raw, otherwise some necessary data wouldn't be added
final syslog message.