'use strict';

var util = require('util');
var dgram = require('dgram');
var dns = require('dns');
var Writable = require('stream').Writable;

function UdpClientStream(host, port, onReady) {
  Writable.call(this);
  this.port = port;
  this.socket = dgram.createSocket('udp4');
  this.socket.on('close', this.emit.bind(this, 'close'));
  this.socket.on('error', this.emit.bind(this, 'error'));
  this.once('finish', function () {
    this.socket.close();
  });

  dns.lookup(host, function (err, address, family) {
    if (err) {
      return onReady(err);
    }

    this.ip = address;
    onReady();
  }.bind(this));
}

util.inherits(UdpClientStream, Writable);

UdpClientStream.prototype._write = function (msg, encoding, callback) {
  this.socket.send(msg, 0, msg.length, this.port, this.ip, callback);
};

module.exports = {
  UdpWriteStream: UdpClientStream
};



