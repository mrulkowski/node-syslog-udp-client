'use strict';

var util = require('util');
var safeJsonStringify = require('safe-json-stringify');
var Transform = require('stream').Transform;

var SYSLOG_VERSION = 1;
/**
 * @param {Object} options
 * @param {Number} options.facility
 * @param {String} options.appName
 * @param {Function} options.levelMapping
 * @constructor
 */
function TransformBunyanEvent(options) {
  Transform.call(this, {objectMode: true});

  this.facility = options.facility;
  this.appName = options.appName;
  this.levelMapping = options.levelMapping;
}

util.inherits(TransformBunyanEvent, Transform);

TransformBunyanEvent.prototype._transform = function (event, encoding, next) {
  var level = this.levelMapping(event.level);
  var priority = '<' + (this.facility * 8 + level) + '>';
  var timestamp = new Date(event.time).toJSON();
  var hostname = event.hostname;
  var messageId = '-';
  var structuredData = '-';
  var header = priority +
    SYSLOG_VERSION + ' ' +
    timestamp + ' ' +
    hostname + ' ' +
    this.appName + ' ' +
    event.pid + ' ' +
    messageId + ' ' +
    structuredData;


  var serializedEvent = safeJsonStringify(event);
  var buffer = new Buffer(header + ' ' + serializedEvent, 'utf-8');
  this.push(buffer, 'utf-8');

  next();
};

module.exports = {
  TransformBunyanEvent: TransformBunyanEvent
};
