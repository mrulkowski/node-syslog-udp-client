'use strict';

var PORT = 14233;

var chai = require('chai');
var expect = chai.expect;
var dgram = require('dgram');

var UdpWriteStream = require('../../syslog/write-stream').UdpWriteStream;

describe('Udp client message sending', function () {
  var server;

  before(function () {
    server = dgram.createSocket('udp4');
    server.bind(PORT);
  });

  after(function () {
    server.close();
  });


  describe('simple message is sent', function () {
    var client;
    var messageValue = 'test';
    var receivedPayload = null;

    before(function (done) {
      server.once('message', function (message) {
        receivedPayload = message;
      });

      client = new UdpWriteStream('localhost', PORT, function (err) {
        if (err) {
          return done(err);
        }

        client.write(messageValue);
        client.end(function () {
          done();
        });
      });
    });

    it('and received', function () {
      var expectedPayload = new Buffer(messageValue, 'utf-8');
      expect(expectedPayload).to.be.deep.equal(receivedPayload);
    });
  });
});
