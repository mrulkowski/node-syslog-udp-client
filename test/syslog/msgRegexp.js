'use strict';

var os = require('os');


module.exports = {
  createRegexp: function (priotity, appName) {
    var prefix = '<' + priotity + '>1';
    var dateFormat = '\\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}\\.\\d{1,3}Z';
    var hostname = os.hostname();
    var pid = process.pid;

    return new RegExp(prefix + ' ' + dateFormat + ' ' + hostname + ' ' +
    appName + ' ' + pid + ' - - .*');

  }
};