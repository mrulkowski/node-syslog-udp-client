'use strict';

var PORT = 19001;

var chai = require('chai');
var expect = chai.expect;
var dgram = require('dgram');
var through2 = require('through2');
var bunyan = require('bunyan');

var syslogLevel = require('../../syslog/level');
var bunyanStream = require('../../stream/bunyan');

describe('Complete stream sends formatted messages', function () {
  var server;
  var dummyMessage = 'Dummy message';
  var testFormatter = through2.obj(function (chunk, enc, callback) {
    this.push(dummyMessage);
    callback();
  });

  before(function () {
    server = dgram.createSocket('udp4');
    server.bind(PORT);
  });

  after(function () {
    server.close();
  });

  describe('that are received', function () {
    var logger;
    var receivedPayload = null;
    before(function () {
      var stream = bunyanStream.createStream({
        host: 'localhost',
        port: PORT,
        appName: 'unit-test',
        levelMapping: function () {
          return syslogLevel.DEBUG;
        }
      }, testFormatter);

      logger = bunyan.createLogger({
        name: 'unit-test-full',
        streams: [{
          type: 'raw',
          level: 'info',
          stream: stream
        }]
      });

      server.once('message', function (message) {
        receivedPayload = message;
      });

      logger.info({success: true}, 'simple message');
    });

    it('formatted message is received by server', function () {
      var expectedPayload = new Buffer(dummyMessage, 'utf-8');
      expect(expectedPayload).to.be.deep.equal(receivedPayload);
    });
  });
});
