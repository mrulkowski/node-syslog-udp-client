'use strict';

var sendLogs = require('pumpify');
var syslogFacility = require('../syslog/facility');

var FromBunyan = require('../syslog/formatter/bunyan').TransformBunyanEvent;
var UdpClient = require('../syslog/write-stream').UdpWriteStream;

module.exports = {
  /**
   * Create stream that will be added to streams ofr bunyan logger
   *
   * @param {Object} options
   * @param {String} options.host
   * @param {Number} options.port
   * @param {Number} [options.facility] - default is user facility
   * @param {String} options.appName
   * @param {Function} options.levelMapping
   * @param {Transform} [formatter] - transform stream from bunyan event object
   *                                  to syslog message.
   */
  createStream: function (options, formatter) {
    var transform;
    if (formatter) {
      transform = formatter;
    } else {
      transform = new FromBunyan({
        appName: options.appName,
        facility: options.facility || syslogFacility.USER,
        levelMapping: options.levelMapping
      });
    }

    var udpClient = new UdpClient(options.host, options.port, function (err) {
      if (err) {
        udpClient.emit('error', err);
      }

      udpClient.uncork();
    });

    udpClient.cork();

    return sendLogs.obj(transform, udpClient);
  }
};