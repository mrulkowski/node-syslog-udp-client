'use strict';

module.exports = {
  syslogLevel: require('./syslog/level'),
  syslogFacility: require('./syslog/facility'),

  bunyan: require('./stream/bunyan')
};